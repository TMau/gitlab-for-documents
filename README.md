# Gitlab für Texte

... in Schule, Studium und Wissenschaft!

## Warum und wie sieht das aus/wie fühlt sich das an?

[Ein kurzes Video als Einführung](https://youtu.be/i8MxZU9w6bc)


## Klingt gut? - Hier gehts zum Kurs in [HTML](https://axel-klinger.gitlab.io/gitlab-for-documents/index.html), [PDF](https://axel-klinger.gitlab.io/gitlab-for-documents/course.pdf) oder als [EPUB](https://axel-klinger.gitlab.io/gitlab-for-documents/course.epub)
